import json
import httplib

class COSM:
   server = "api.cosm.com"
   feedid = ""
   key = ""
   logging = None

   def loadConfig(self,filename):
      f = open(filename,'rb')
      jsonStr = f.read()
      f.close()
      config = json.loads(jsonStr)
      if "feed_id" in config:
         self.setFeedId(config["feed_id"])
         self.logInfo("loaded COSM feed ID")
      if "key" in config:
         self.setKey(config["key"])
         self.logInfo("loaded COSM key")

   def setFeedId(self,feedid):
      self.feedid = feedid
   
   def setKey(self,key):
      self.key = key

   def setLogging(self,logging):
      self.logging = logging

   def logInfo(self,msg):
      if self.logging is not None:
            self.logging.info(msg)

   def logDebug(self,msg):
      if self.logging is not None:
            self.logging.debug(msg)

   def http(self,url,data,method='POST'):
      r = None
      headers = {
         "Content-Type"  : "application/x-www-form-urlencoded",
         "X-ApiKey"      : self.key,
         "User-Agent"    : "python",
      }
      try:
         h = httplib.HTTPSConnection(self.server)
         h.request(method,url,data,headers)
         r = h.getresponse()
      except ex:
         self.logInfo("HTTP error: %s"%ex)
      return r

   def prepareDatastreams(self,datastreams):
      if datastreams is None:
         return

      url = "/v2/feeds/%s/"%(self.feedid)
      r = self.http(url,None,'GET')
      if r is None:
         return
      if r.status==200:
         jsonStr = r.read()
         feed = json.loads(jsonStr)
         ids = []
         if 'datastreams' in feed:
            for datastream in feed['datastreams']:
               ids.append(datastream['id'])
         ids = sorted(set(datastreams)-set(ids))
         self.createDatastream(ids)
         
   def createDatastream(self,datastreams):
      if datastreams is None:
         return
      if len(datastreams)<1:
         return
      self.logInfo("Creating datastreams: %s"%datastreams)

      data = {}
      data["version"] = "1.0.0"
      data["datastreams"] = []
      for datastream in datastreams:
         data["datastreams"].append({"id":datastream})
      data = json.dumps(data)

      url = "/v2/feeds/%s/datastreams"%(self.feedid)
      r = self.http(url,data)
      if r is None:
         return
      if r.status == 201:
          self.logInfo("datastreams created")
      else:
          self.logInfo("datastreams NOT created [Status:%d]"%r.status)

   def uploadDatapoints(self,datastream,datapoints):
      if datapoints is None:
         return

      url = "/v2/feeds/%s/datastreams/%s/datapoints"%(self.feedid,datastream)
      
      data = {}
      data['datapoints'] = []
      for datapoint in datapoints:
         d_point = {'at':datapoint['at'].isoformat(),'value':datapoint['value']}
         data['datapoints'].append(d_point)
      data = json.dumps(data)
      self.logDebug(data)

      r = self.http(url,data)
      if r is None:
         return 0
      self.logDebug("Upload datapoints. HTTP Status: %s %s"%(r.status,r.reason))
      return r.status
