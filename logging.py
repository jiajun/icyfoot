import datetime

class Logging:

   logLevel = 1
   screenOutput = False

   def setLogLevel(self, logLevel):
      self.logLevel = logLevel
      self.__log("LOG LEVEL: %d"%logLevel,True)

   def setOutputScreen(self, screenOutput):
      self.screenOutput = screenOutput
      if screenOutput==True:
         self.info("Output to screen enabled")

   def info(self,message,screen=False):
      if self.logLevel>=1:
         self.__log(message,screen)

   def debug(self,message,screen=False):
      if self.logLevel>=2:
         self.__log(message,screen)

   def __log(self,message,screen=False):
      if self.screenOutput==True or screen==True:
         timeStr = self.getTimeString()
         output = "[%s] %s"%(timeStr,message)
         print output

   def getTimeString(self):
      str = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
      return str
