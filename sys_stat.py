import sys
import Queue
import psutil
import datetime
import threading
from time import sleep
from logging import Logging
from cosm import COSM

def init(refreshInterval,uploadInterval,statLast,statQueue,cosm):
   refreshThreadArgs = (refreshInterval,statLast,statQueue)
   refreshThread = threading.Thread(target=refresh,args=refreshThreadArgs)
   refreshThread.daemon = True
   uploadThreadArgs = (uploadInterval, cosm, statQueue)
   uploadThread = threading.Thread(target=upload,args=uploadThreadArgs)
   uploadThread.daemon = True
   net = psutil.network_io_counters(True)
   net_time = datetime.datetime.utcnow()
   statLast['net'] = {'value':net,'at':net_time}
   refreshThread.start()
   uploadThread.start()

def initQueue(statQueue,labels):
   for label in labels:
      statQueue[label] = Queue.Queue()

def upload(interval, cosm,statQueue):
   logging.info("thread <upload> started [interval:%d]"%interval)
   while True:
      sleep(interval)
      for feed in statQueue:
         if statQueue[feed].empty()==True:
            continue

         datapoints = []

         # prepare datapoints. max 500
         count = 0
         while count<500 and statQueue[feed].empty()==False:
            datapoints.append(statQueue[feed].get())
            count = count + 1

         # try to upload. max 3 times
         status = 0
         for i in range(3):
            status = cosm.uploadDatapoints(feed,datapoints)         
            if status==200:
               break
         if status != 200:
            for datapoint in datapoints:
               statQueue[feed].put(datapoint)
            logging.info("Upload feed:%s failed! [status:%d]"%(feed,status))
         else:
            logging.info("Upload feed:%s okay!"%feed) 

def refresh(interval,statLast,statQueue):
   logging.info("thread <refresh> started [interval:%d]"%interval)
   sleep(interval)
   while True:
      logging.info("refreshing...")
      refresh_cpu(statQueue)
      refresh_memory(statQueue)
      refresh_network(statLast,statQueue)
      refresh_temperature(statQueue)
         
      #sleep for interval second
      sleep(interval)


def refresh_cpu(statQueue):
      #cpu percent
      cpu_usage = psutil.cpu_percent(interval=0)
      cpu_time = datetime.datetime.utcnow()
      statQueue['cpu_usage'].put({'value':cpu_usage,'at':cpu_time})
      logging.debug("cpu usage=%f%%"%(cpu_usage))

def refresh_memory(statQueue):
      #memory available (divide by 1024 to get K)
      mem_free = psutil.phymem_usage().free
      mem_free = mem_free / 1024.0 / 1024.0 #M
      mem_time = datetime.datetime.utcnow()
      statQueue['memory_free'].put({'value':mem_free,'at':mem_time})
      logging.debug("mem free=%fMiB"%mem_free)

def refresh_temperature(statQueue):
      #temperature
      f = open('/sys/class/thermal/thermal_zone0/temp','rb')
      temp_str = f.readline()
      f.close()
      if temp_str is not None:
         temp_str = temp_str.strip()
         temp_int = int(temp_str)/1000.0
         temp_time = datetime.datetime.utcnow()
         statQueue['temperature'].put({'value':temp_int,'at':temp_time})
         logging.debug("temperature=%f deg Celsius"%temp_int)

def refresh_network(statLast,statQueue):
      #network stat
      netstat_last = statLast['net']['value']
      netstat = psutil.network_io_counters(True)
      netstat_time = datetime.datetime.utcnow()
      time_delta = netstat_time-statLast['net']['at']
      time_delta = time_delta.total_seconds()
      for intf in netstat:
         if intf not in netstat_last:
            continue
         if intf not in ['eth0']:
            continue
         recv = netstat[intf].bytes_recv-netstat_last[intf].bytes_recv
         recv = (recv*8)/1000.0/time_delta #kbps
         sent = netstat[intf].bytes_sent-netstat_last[intf].bytes_sent
         sent = (sent*8)/1000.0/time_delta #kbps
         statQueue[intf+'_recv'].put({'value':recv,'at':netstat_time})
         statQueue[intf+'_sent'].put({'value':sent,'at':netstat_time})
         logging.debug("%s recv:%fkbps sent:%fkbps [%f]"%(intf,recv,sent,time_delta))
      statLast['net']={'value':netstat,'at':netstat_time}

logLevel = 1
if len(sys.argv)>1:
   if sys.argv[1] in ["1","2"]:
      logLevel = int(sys.argv[1])

logging = Logging()
logging.setLogLevel(logLevel)
logging.setOutputScreen(True)

indicators = ['cpu_usage','memory_free','temperature','eth0_recv','eth0_sent']
logging.info("Logging: %s"%indicators)
statQueue = {}
initQueue(statQueue,indicators)
statLast = {}

cosm = COSM()
cosm.setLogging(logging)
cosm.loadConfig("cosmConfig.json")
cosm.prepareDatastreams(indicators)

refreshInterval = 5
uploadInterval = 300
init(refreshInterval,uploadInterval,statLast,statQueue,cosm)
while True:
   sleep(3)
